#!/usr/bin/env bash

. $(dirname $0)/common.sh

DATASET_DIR=$1;#"/vagrant/data/input/dataset/";
PROCESS_DIR=$2;#"/vagrant/data/input/processing/"
echo $DATASET_DIR;
echo $PROCESS_DIR;

IFS='
'

#curl -XDELETE 'http://10.10.10.10:9200/kpi*'

start_time=`date +%s`
date=$(TZ=UTC date "+%Y-%m-%d_%H")

#echo -e "$(date): Starting main flow..."
## If files are found in dataset dir
#if [ "$(ls -A $DATASET_DIR)" ]; then
#    echo "$(date): Dataset folder not empty, with size $(du -h $DATASET_DIR | cut -f -1)";
#    # Loop through dataset items
#    for file in $DATASET_DIR*; do
#        echo "$(date): Cleaning up $(basename $file)";
#        pre_process_file $file $PROCESS_DIR;
#    done
#else
#    echo "$(date): No files to process, quiting...";
#    exit 0;
#fi
#echo "$(date): Finished formating input files"
##### Submit newly created files for processing #####
spark_start_time=`date +%s`

if [ "$(ls -A $PROCESS_DIR)" ]; then
    spark_submit $PROCESS_DIR;
else
    echo "$date No files to submit, quiting...";
fi

spark_end_time=`date +%s`
echo "$(date): Finished Spark processing"
##### Cleaning up #####
cleanup_start_time=`date +%s`

echo "$date Clean input directory";
#rm -rf $DATASET_DIR*

echo "$date Clean processing directory";
#rm -rf $PROCESS_DIR*

##### Statistics #####
end_statistics $start_time $spark_start_time $spark_end_time $cleanup_start_time
