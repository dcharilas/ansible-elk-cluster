#!/usr/bin/env bash

pre_process_file() {
    sed '1d' $1 | \
    awk -F, -v OFS="," '{$30=substr($30,0,8); print}' | \
    awk -F, -v OFS="," '{ if ($3 == $5) {$3=1; print} else {$3=0; print}}' | \
    cut -d "," -f3,17,22,24-31,33-36 > \
    $2$(basename $1).todo
}

spark_submit() {
    echo "$(date): Submitting spark job for $(basename $1)"
    spark-submit --class Aggregation \
    --master spark://aggregator:7077 \
    --driver-memory 3g \
    /vagrant/scripts/aggregator/target/aggregator.jar \
    $1
    echo "$(date): Spark job finished for $(basename $1)"
}

end_statistics() {
    end=`date +%s`
    total_time=$((end-$1))
    preprocess_time=$(($2-$1))
    spark_time=$(($3-$2))
    cleanup_time=$(($4-$3))
    echo "$(date): Preprocess took $preprocess_time seconds";
    echo "$(date): Spark processing took $spark_time seconds";
    echo "$(date): Cleanup took $cleanup_time seconds";
    echo "$(date): Finished after $total_time seconds";
    echo "$(date): That's all folks!"
}
