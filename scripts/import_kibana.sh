#!/bin/sh

count="$(cat $1 | jq '.hits.hits | length')"
es_host=$2

i=0
while [ "$i" -lt $count ]; do
    body=$(cat $1 | jq -r --arg v "${i}" '.hits.hits[$v|tonumber]._source');
    type=$(cat $1 | jq -r --arg v "${i}" '.hits.hits[$v|tonumber]._type');
    id=$(cat $1 | jq -r --arg v "${i}" '.hits.hits[$v|tonumber]._id');
    curl -XPUT "$es_host/.kibana/$type/$id" -d "$body"
    i=$(( i + 1 ))
done