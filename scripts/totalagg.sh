#!/usr/bin/env bash

spark_submit() {
    echo "$(date): Perform index aggregation"
    spark-submit --class AggregationES \
    --master spark://aggregator:7077 \
    --driver-memory 3g \
    /vagrant/scripts/aggregator/target/aggregator.jar \
    $1
    echo "$(date): Finished index aggregation"
}

spark_submit;


