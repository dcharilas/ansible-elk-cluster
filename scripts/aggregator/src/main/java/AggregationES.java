import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;
import util.SQLContextAggregations;

/**
 * Class that reads data from ES, applies Spark aggregations and writes to ES
 *
 * Created by dcharilas
 */
public class AggregationES {

    private static String esHost = "10.10.10.10";
    private static String esPort = "9200";


    public static void main(String [] args) {
        /*
         * Read arguments
         */
        if (args != null && args.length > 0) {
            if (args[0] != null) {
                esHost = args[0];
            }
            if (args.length > 1) {
                esPort = args[1];
            }
        }
        /*
         * winutils.exe should be downloaded and placed in this path:  C://Windows/bin
         */
        if ("Windows 7".equalsIgnoreCase(System.getProperty("os.name"))) {
            System.setProperty("hadoop.home.dir", "C://Windows");
            System.setProperty("spark.sql.warehouse.dir", "file:///c:/Spark/spark-2.0.0-bin-hadoop2.7/spark-warehouse");
        }
        /*
         * Set up configuration
         */
        SparkConf conf = new SparkConf().setAppName("Aggregation").setMaster("local");
        conf.set("spark.serializer", org.apache.spark.serializer.KryoSerializer.class.getName());
        conf.set("es.index.auto.create", "true");
        conf.set("es.nodes", esHost);
        conf.set("es.port", esPort);
        conf.set("es.batch.size.entries", "40000");
        conf.set("es.batch.size.bytes", "40mb");
        conf.set("indices.memory.index_buffer_size","20");
        conf.set("index.refresh_interval","30");
        conf.set("index.merge.scheduler.max_thread_count","1");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        /*
         * Read data from Elasticsearch, apply aggregation and write back to ES
         */
        Dataset df;
        Dataset aggregators;

        /*
         * For kpi_aggregator
         */
        df = JavaEsSparkSQL.esDF(sqlContext, "kpi_aggregator/record");
        df.registerTempTable("data");
        aggregators = sqlContext.sql(SQLContextAggregations.OPERATORS_DB);
        JavaEsSparkSQL.saveToEs(aggregators, "kpi_aggregator_total/record");

        /*
         * For kpi_bytes
         */
        df = JavaEsSparkSQL.esDF(sqlContext, "kpi_bytes/record");
        df.registerTempTable("data");
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_BYTES_DB);
        JavaEsSparkSQL.saveToEs(aggregators, "kpi_bytes_total/record");

        /*
         * For kpi_devices
         */
        df = JavaEsSparkSQL.esDF(sqlContext, "kpi_devices/record");
        df.registerTempTable("data");
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_DEVICES_DB);
        JavaEsSparkSQL.saveToEs(aggregators, "kpi_devices_total/record");


    }

}