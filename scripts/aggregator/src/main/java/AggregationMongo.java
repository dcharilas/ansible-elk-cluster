import com.mongodb.spark.MongoSpark;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.StructType;
import util.AggregatorComponents;
import util.SQLContextAggregations;


/**
 * Class that reads CSV, applies Spark aggregations and writes to MongoDB
 *
 * Created by dcharilas
 */
public class AggregationMongo {

    private static String mongoHost = "10.10.10.30";
    private static String database = "demodb";
    private static String logFile = "E:\\workspace\\ansible-elk-cluster\\data\\input\\processing";


    public static void main(String [] args) {
        /*
         * Read arguments
         */
        if (args != null && args.length > 0) {
            if (args[0] != null) {
                logFile = args[0];
            }
            if (args.length > 1) {
                mongoHost = args[1];
            }
            if (args.length > 2) {
                database = args[2];
            }
        }
        final String uri = "mongodb://" +mongoHost +":27017/" +database;
        /*
         * winutils.exe should be downloaded and placed in this path:  C://Windows/bin
         */
        if ("Windows 7".equalsIgnoreCase(System.getProperty("os.name"))) {
            System.setProperty("hadoop.home.dir", "C://Windows");
            System.setProperty("spark.sql.warehouse.dir", "file:///c:/Spark/spark-2.0.0-bin-hadoop2.7/spark-warehouse");
        }
        /*
         * Set up configuration
         */
        SparkConf conf = new SparkConf().setAppName("Aggregation").setMaster("local");
        conf.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
        conf.set("mongo.input.uri", uri);
        conf.set("mongo.output.uri", uri);
        JavaSparkContext sc = new JavaSparkContext(conf);
        /*
         * Declare schema
         */
        SQLContext sqlContext = new SQLContext(sc);
        StructType customSchema = AggregatorComponents.getCsvSchema();
        /*
         * Read CSV and apply schema
         */
        Dataset df = sqlContext.read()
                .format("com.databricks.spark.csv")
                .schema(customSchema)
                .option("header", "false")
                .load(logFile);

        df.registerTempTable("data");

        df.cache();

        /*
         * Aggregation operators
         */
        Dataset aggregators;
        aggregators = sqlContext.sql(SQLContextAggregations.OPERATORS_CSV);
        MongoSpark.write(aggregators).option("uri", uri).option("collection", "kpi_aggregator").option("mode", "append").save();

        /*
         *  Kpi bytes
         */
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_BYTES_CSV);
        MongoSpark.write(aggregators).option("uri", uri).option("collection", "kpi_bytes").option("mode", "append").save();

        /*
         * Kpi devices
         */
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_DEVICES_CSV);
        MongoSpark.write(aggregators).option("uri", uri).option("collection", "kpi_devices").option("mode", "append").save();


    }

}