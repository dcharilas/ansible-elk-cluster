package util;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

/**
 * Created by dcharilas on 9/10/2016.
 */
public class AggregatorComponents {


    public static StructType getCsvSchema() {
        return new StructType(new StructField[] {
                new StructField("active_flow_flag", DataTypes.IntegerType, true, Metadata.empty()),
                new StructField("tcp_rst", DataTypes.IntegerType, true, Metadata.empty()),
                new StructField("bytes_uplink", DataTypes.IntegerType, true, Metadata.empty()),
                new StructField("bytes_downlink", DataTypes.IntegerType, true, Metadata.empty()),
                new StructField("service_id", DataTypes.StringType, true, Metadata.empty()),
                new StructField("apn", DataTypes.StringType, true, Metadata.empty()),
                new StructField("home_mcc", DataTypes.StringType, true, Metadata.empty()),
                new StructField("home_mnc", DataTypes.StringType, true, Metadata.empty()),
                new StructField("cell_id", DataTypes.StringType, true, Metadata.empty()),
                new StructField("tac", DataTypes.StringType, true, Metadata.empty()),
                new StructField("imsi", DataTypes.StringType, true, Metadata.empty()),
                new StructField("rat", DataTypes.StringType, true, Metadata.empty()),
                new StructField("host_server_name", DataTypes.StringType, true, Metadata.empty())
        });
    }
}
