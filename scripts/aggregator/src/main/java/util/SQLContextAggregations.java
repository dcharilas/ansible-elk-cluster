package util;

/**
 * Created by dcharilas on 9/10/2016.
 */
public class SQLContextAggregations {

    public static final String OPERATORS_CSV = "SELECT service_id, host_server_name, imsi, tac, rat, apn, home_mcc, home_mnc, cell_id, " +
            "SUM(bytes_uplink) as sum_bytes_uplink, SUM(bytes_downlink) as sum_bytes_downlink, SUM(tcp_rst) as sum_tcp_rst, " +
            "SUM(active_flow_flag) as active_flow_count, SUM(case active_flow_flag when 0 then 1 else 0 end) as new_flow_count,  " +
            "CONCAT(service_id,',',host_server_name,',',imsi,',',tac,',',rat,',',apn,',',home_mcc,',',home_mnc,',',cell_id) as groupby_clause " +
            "FROM data GROUP BY service_id, host_server_name, imsi, tac, rat, apn, home_mcc, home_mnc, cell_id";


    public static final String KPI_BYTES_CSV = "SELECT tac, host_server_name, SUM(bytes_uplink) as sum_bytes_uplink, SUM(bytes_downlink) as sum_bytes_downlink " +
            "FROM data GROUP BY tac, host_server_name";


    public static final String KPI_DEVICES_CSV = "SELECT apn, home_mcc, home_mnc, cell_id, COUNT(DISTINCT tac) as number_of_tac," +
            "CONCAT(apn,',',home_mcc,',',home_mnc,',',cell_id) as groupby_clause " +
            "FROM data GROUP BY apn, home_mcc, home_mnc, cell_id";


    public static final String OPERATORS_DB = "SELECT service_id, host_server_name, imsi, tac, rat, apn, home_mcc, home_mnc, cell_id, " +
            "SUM(sum_bytes_uplink) as sum_bytes_uplink, SUM(sum_bytes_downlink) as sum_bytes_downlink, SUM(sum_tcp_rst) as sum_tcp_rst, " +
            "SUM(active_flow_count) as active_flow_count, SUM(new_flow_count) as new_flow_count,  " +
            "CONCAT(service_id,',',host_server_name,',',imsi,',',tac,',',rat,',',apn,',',home_mcc,',',home_mnc,',',cell_id) as groupby_clause " +
            "FROM data GROUP BY service_id, host_server_name, imsi, tac, rat, apn, home_mcc, home_mnc, cell_id";


    public static final String KPI_BYTES_DB = "SELECT tac, host_server_name, SUM(sum_bytes_uplink) as sum_bytes_uplink, SUM(sum_bytes_downlink) as sum_bytes_downlink " +
            "FROM data GROUP BY tac, host_server_name";


    public static final String KPI_DEVICES_DB = "SELECT apn, home_mcc, home_mnc, cell_id, SUM(number_of_tac) as number_of_tac," +
            "CONCAT(apn,',',home_mcc,',',home_mnc,',',cell_id) as groupby_clause " +
            "FROM data GROUP BY apn, home_mcc, home_mnc, cell_id";
}
