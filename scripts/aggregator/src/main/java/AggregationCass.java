import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.StructType;
import util.AggregatorComponents;

/**
 * Class that reads CSV, applies Spark aggregations and writes to Cassandra
 *
 * Created by dcharilas
 */

// TODO this class is under construction
public class AggregationCass {

    private static String cassHost = "10.10.10.30";
    private static String logFile = "E:\\workspace\\ansible-elk-cluster\\data\\input\\processing";


    public static void main(String [] args) {
        /*
         * Read arguments
         */
        if (args != null && args.length > 0) {
            if (args[0] != null) {
                logFile = args[0];
            }
            if (args.length > 1) {
                cassHost = args[1];
            }
        }
        /*
         * winutils.exe should be downloaded and placed in this path:  C://Windows/bin
         */
        if ("Windows 7".equalsIgnoreCase(System.getProperty("os.name"))) {
            System.setProperty("hadoop.home.dir", "C://Windows");
            System.setProperty("spark.sql.warehouse.dir", "file:///c:/Spark/spark-2.0.0-bin-hadoop2.7/spark-warehouse");
        }
        /*
         * Set up configuration
         */
        SparkConf conf = new SparkConf().setAppName("Aggregation").setMaster("local");
        conf.set("spark.cassandra.connection.host", cassHost);
        JavaSparkContext sc = new JavaSparkContext(conf);
        /*
         * Declare schema
         */
        SQLContext sqlContext = new SQLContext(sc);
        StructType customSchema = AggregatorComponents.getCsvSchema();
        /*
         * Read CSV and apply schema
         */
        Dataset df = sqlContext.read()
                .format("com.databricks.spark.csv")
                .schema(customSchema)
                .option("header", "false")
                .load(logFile);

        df.registerTempTable("data");


        //TODO this should be moved in initialisation
        /*
         * Create schema in Cassandra
         */
        CassandraConnector connector = CassandraConnector.apply(sc.getConf());
        Session session = connector.openSession();
        session.execute("DROP KEYSPACE IF EXISTS demo");
        session.execute("CREATE KEYSPACE demo WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}");
        session.execute("CREATE TABLE demo.kpi_aggregator (service_id TEXT, host_server_name TEXT, imsi TEXT, tac TEXT, rat TEXT,"+
         "apn TEXT, home_mcc TEXT, home_mnc TEXT, cell_id TEXT, sum_bytes_uplink BIGINT, sum_bytes_downlink BIGINT,"+
         "sum_tcp_rst BIGINT, active_flow_count BIGINT, new_flow_count BIGINT, groupby_clause TEXT");
        session.execute("CREATE TABLE demo.kpi_bytes (tac TEXT, host_server_name TEXT, sum_bytes_uplink BIGINT, sum_bytes_downlink BIGINT)");
        session.execute("CREATE TABLE demo.kpi_devices (apn TEXT, home_mcc TEXT, home_mnc TEXT, cell_id TEXT, number_of_tac INT, groupby_clause TEXT");

        /*
         * Write to Cassandra
         */
        //javaFunctions(kpi1.toJavaRDD()).writerBuilder("demo", "kpi_bytes", mapToRow(Person.class)).saveToCassandra();

    }

}