import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.ReadConfig;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.StructType;
import util.AggregatorComponents;
import util.SQLContextAggregations;

import java.util.HashMap;
import java.util.Map;


/**
 * Class that reads CSV, applies Spark aggregations and writes to MongoDB
 *
 * Created by dcharilas
 */
public class AggregationMongoRead {

    private static String mongoHost = "10.10.10.30";
    private static String database = "demodb";


    public static void main(String [] args) {
        /*
         * Read arguments
         */
        if (args != null && args.length > 0) {
            if (args[0] != null) {
                mongoHost = args[0];
            }
            if (args.length > 1) {
                database = args[1];
            }
        }
        final String uri = "mongodb://" +mongoHost +":27017/" +database;
        /*
         * winutils.exe should be downloaded and placed in this path:  C://Windows/bin
         */
        if ("Windows 7".equalsIgnoreCase(System.getProperty("os.name"))) {
            System.setProperty("hadoop.home.dir", "C://Windows");
            System.setProperty("spark.sql.warehouse.dir", "file:///c:/Spark/spark-2.0.0-bin-hadoop2.7/spark-warehouse");
        }
        /*
         * Set up configuration
         */
        SparkConf conf = new SparkConf().setAppName("Aggregation").setMaster("local");
        conf.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
        conf.set("mongo.input.uri", uri);
        conf.set("mongo.output.uri", uri);
        conf.set("spark.mongodb.input.partitioner","MongoPaginateBySizePartitioner");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);
        /*
         * Read data from MongoDB, apply aggregation and write back to ES
         */
        Dataset df;
        Dataset aggregators;

        Map<String, String> options = new HashMap<String, String>();
        options.put("uri", uri);
        options.put("database", database);

        /*
         * For kpi_aggregator
         */
        df = MongoSpark.read(sqlContext).options(options).option("collection", "kpi_aggregator").load();
        df.registerTempTable("data");
        aggregators = sqlContext.sql(SQLContextAggregations.OPERATORS_DB);
        MongoSpark.write(aggregators).option("uri", uri).option("collection", "kpi_aggregator_total").option("mode", "append").save();

        /*
         * For kpi_bytes
         */
        df = MongoSpark.read(sqlContext).options(options).option("collection", "kpi_bytes").load();
        df.registerTempTable("data");
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_BYTES_DB);
        MongoSpark.write(aggregators).option("uri", uri).option("collection", "kpi_bytes_total").option("mode", "append").save();

        /*
         * For kpi_devices
         */
        df = MongoSpark.read(sqlContext).options(options).option("collection", "kpi_devices").load();
        df.registerTempTable("data");
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_DEVICES_DB);
        MongoSpark.write(aggregators).option("uri", uri).option("collection", "kpi_devices_total").option("mode", "append").save();

    }

}