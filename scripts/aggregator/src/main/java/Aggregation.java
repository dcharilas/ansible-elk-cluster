import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.StructType;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;
import util.AggregatorComponents;
import util.SQLContextAggregations;

/**
 * Class that reads CSV, applies Spark aggregations and writes to ES
 *
 * Created by dcharilas
 */
public class Aggregation {

    private static String esHost = "10.10.10.10";
    private static String esPort = "9200";
    private static String logFile = "E:/workspace/ansible-elk-cluster/data/input/processing";


    public static void main(String [] args) {
        /*
         * Read arguments
         */
        if (args != null && args.length > 0) {
            if (args[0] != null) {
                logFile = args[0];
            }
            if (args.length > 1) {
                esHost = args[1];
            }
            if (args.length > 2) {
                esPort = args[2];
            }
        }
        /*
         * winutils.exe should be downloaded and placed in this path:  C://Windows/bin
         */
        if ("Windows 7".equalsIgnoreCase(System.getProperty("os.name"))) {
            System.setProperty("hadoop.home.dir", "C://Windows");
            System.setProperty("spark.sql.warehouse.dir", "file:///c:/Spark/spark-2.0.0-bin-hadoop2.7/spark-warehouse");
        }
        /*
         * Set up configuration
         */
        SparkConf conf = new SparkConf().setAppName("Aggregation").setMaster("local");
        conf.set("spark.serializer", org.apache.spark.serializer.KryoSerializer.class.getName());
        conf.set("es.index.auto.create", "true");
        conf.set("es.nodes", esHost);
        conf.set("es.port", esPort);
        conf.set("es.batch.size.entries", "40000");
        conf.set("es.batch.size.bytes", "40mb");
        conf.set("indices.memory.index_buffer_size","20");
        conf.set("index.refresh_interval","30");
        conf.set("index.merge.scheduler.max_thread_count","1");
        JavaSparkContext sc = new JavaSparkContext(conf);
        /*
         * Declare schema
         */
        SQLContext sqlContext = new SQLContext(sc);
        StructType customSchema = AggregatorComponents.getCsvSchema();
        /*
         * Read CSV and apply schema
         */
        Dataset df = sqlContext.read()
                .format("com.databricks.spark.csv")
                .schema(customSchema)
                .option("header", "false")
                .load(logFile);

        df.registerTempTable("data");

        df.cache();

        /*
         * Aggregation operators
         */
        Dataset aggregators;
        aggregators = sqlContext.sql(SQLContextAggregations.OPERATORS_CSV);
        JavaEsSparkSQL.saveToEs(aggregators, "kpi_aggregator/record");

        /*
         *  Kpi bytes
         */
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_BYTES_CSV);
        JavaEsSparkSQL.saveToEs(aggregators, "kpi_bytes/record");

        /*
         * Kpi devices
         */
        aggregators = sqlContext.sql(SQLContextAggregations.KPI_DEVICES_CSV);
        JavaEsSparkSQL.saveToEs(aggregators, "kpi_devices/record");


    }

}