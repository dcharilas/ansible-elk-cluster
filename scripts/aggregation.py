from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import *

sc = SparkContext()


# Define schema
sqlContext = SQLContext(sc)
customSchema = StructType([ \
    StructField("tcp_rst", IntegerType(), True), \
    StructField("bytes_uplink", IntegerType(), True), \
    StructField("bytes_downlink", IntegerType(), True), \
    StructField("service_id", StringType(), True), \
    StructField("apn", StringType(), True), \
    StructField("home_mcc", StringType(), True), \
    StructField("home_mnc", StringType(), True), \
    StructField("cell_id", StringType(), True), \
    StructField("tac", StringType(), True), \
    StructField("imsi", StringType(), True), \
    StructField("rat", StringType(), True), \
    StructField("host_server_name", StringType(), True)])

# Read file
df = sqlContext.read \
    .format('com.databricks.spark.csv') \
    .options(header='true') \
    .load('/vagrant/data/input/input.csv', schema = customSchema)

# Print number of lines
print('Text lines in file: %s' % df.count())

df.registerTempTable("data")

# Aggregation operators
aggregators = sqlContext.sql("SELECT service_id, host_server_name, imsi, tac, rat, apn, home_mcc, home_mnc, cell_id, SUM(bytes_uplink), SUM(bytes_downlink), SUM(tcp_rst) FROM data GROUP BY service_id, host_server_name, imsi, tac, rat, apn, home_mcc, home_mnc, cell_id")

# KPIs
kpi1 = sqlContext.sql("SELECT tac, host_server_name, SUM(bytes_uplink), SUM(bytes_downlink) FROM data GROUP BY tac, host_server_name")
kpi2 = sqlContext.sql("SELECT apn, home_mcc, home_mnc, cell_id, COUNT(tac) FROM data GROUP BY apn, home_mcc, home_mnc, cell_id")

es_conf = {"es.nodes" : "10.10.10.10","es.port" : "9200","es.resource" : "kpi"}
kpi1.rdd.saveAsNewAPIHadoopFile(
    path='-',
    outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat",
    keyClass="org.apache.hadoop.io.NullWritable",
    valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
    conf=es_conf)
# kpi1.write.format("org.elasticsearch.spark.sql")\
#     .option("es.resource", "kpi")\
#     .option("es.nodes", "10.10.10.10")\
#     .save()

