# Big Data Analytics with Vagrant

There are 3 virtual machines defined in this setup:

| Hostname | IP | Role |
| ---------- | ---------- | --------------------------------------- |
| 10.elastic  | 10.10.10.10  | elasticsearch - kibana - nginx |
| 11.elastic  | 10.10.10.11  | elasticsearch |
| aggegator  | 10.10.10.20  | spark |

# Bring up the stack

Run the command:

```shell
$ vagrant up
```

to create all VMs or

```shell
$ vagrant up hostname
```

to create only specified VM

When finished, you should be able to access `kibana` from http://10.10.10.10/app/kibana and see the logs.

If you make changes in the playbook of a VM, run:

```
$ vagrant provision hostname
```

In order to destroy all VMs run

```shell
$ vagrant destroy
```

or to destroy specific VM

```shell
$ vagrant destroy hostname
```

to shutdown a specific VM without destroying it
```shell
$ vagrant halt hostname
```